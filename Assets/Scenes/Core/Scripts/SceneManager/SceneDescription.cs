﻿using UnityEngine;

[System.Serializable]
public class SceneDescription  {
    public int version;
    public string name;
    public string description;
    public string sceneFileName;

    public SceneDescription()
    {
        version = 1;
        name = "";
        description = "";
        sceneFileName = "";
    }
    public static SceneDescription FromJson(string str)
    {
        return JsonUtility.FromJson<SceneDescription>(str);
    }

    public string ToJson()
    {
        return JsonUtility.ToJson(this, true);
    }

}

[System.Serializable]
public class ScenesDescriptionList
{
    public SceneDescription[] descriptions;

    public ScenesDescriptionList(SceneDescription[] ds)
    {
        descriptions = ds;
    }

    public static ScenesDescriptionList FromJson(string str)
    {
        return JsonUtility.FromJson<ScenesDescriptionList>(str);
    }

    public string ToJson()
    {
        return JsonUtility.ToJson(this, true);
    }
}
