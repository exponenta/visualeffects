﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class cSceneManager : MonoBehaviour {

    [SerializeField]
    public ScenesDescriptionList scenes;

    const string descriptionsFile = "SceneDescriptions";

    void Start () {
        GlobalVariables.core_SceneManager = this;
        LoadDescription();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    
    void LoadDescription()
    {
        TextAsset ta = Resources.Load<TextAsset>(descriptionsFile);

        if(ta != null)
        {
            scenes = ScenesDescriptionList.FromJson(ta.text);
        }
    }
}
