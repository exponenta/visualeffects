﻿using UnityEngine;
using System.Collections;
using OpenCVForUnity;
using System.Collections.Generic;

public class GetMotionVectors : MonoBehaviour
{

	public enum Mode {FrameByFrame, FrameByBG, FrameByAccumFrame};
	public enum Detect {Biggest, All, Selections};
    public enum ApproximateMode { Convex, Rect, Elipse};

	WebCamTexture camTexture;
	WebCamDevice webCam;
  
    public Size CameraSize = new Size(640, 480);
	Mat rgbaMat;
	Mat grayMatOld;
	Mat grayMat;
	Mat grayBGMat;
	Mat diffMat;
	Mat accamulator;
    Mat resultMap;


    Color32[] colors;

	Texture2D texture;
    Texture2D texture_2;

    bool initDone = false;

    public Renderer debugOutTex;
    public int InputCamera = 0;
    public Renderer detectOut;
	public bool DebugOut = true;

	public Mode mode = Mode.FrameByFrame;
	public Detect detectMode = Detect.Selections;
    public ApproximateMode approximateMode = ApproximateMode.Convex;

	public Transform screen;
	[Range (1, 255)]
	public int threshold = 25;
	public bool blured = true;
	public int size = 11;
	[Space (10)]
	[Range(0,0.2f)]
	public float minHullSize = 0.002f;
	[Range(0,1)]
	public float accomulateDelay = 0.5f;
    [Range(0, 1)]
    public float accomulateBgSize = 0.5f;

    private List<MatOfPoint> contours = new List<MatOfPoint> ();
	private List<RotatedRect> rects = new List<RotatedRect> ();

	private List<CoreUtils.MotionVector> motionVectors = new List<CoreUtils.MotionVector> ();

	bool callNewBG = false;
	bool bgSetted = false;

	public delegate void OnCameraChange ();
	public static OnCameraChange onCameraChange;

	public delegate void OnDetect (CoreUtils.MotionVector[] vectors);
	public static OnDetect onDetect;

    private int oldCameraIndex = -1;

	public void Start ()
	{
		if (screen == null)
			screen = transform;
		
		if (WebCamTexture.devices.Length > 0)
			StartCoroutine (init (InputCamera));
	}
    
    public void ChangeInputCamera(int index)
    {
        StartCoroutine(init(index));
        
    }
	private IEnumerator init (int cameraIndex)
	{
		if (camTexture != null && cameraIndex % WebCamTexture.devices.Length != oldCameraIndex) {
			camTexture.Stop ();
			initDone = false;
			rgbaMat.Dispose ();
		}
			
		if (WebCamTexture.devices.Length > 0 ) {
            oldCameraIndex = cameraIndex % WebCamTexture.devices.Length;
            webCam = WebCamTexture.devices [oldCameraIndex];
			camTexture = new WebCamTexture (webCam.name, (int)CameraSize.width, (int)CameraSize.height);
            Debug.Log(webCam.name);
		} else {
			yield return null;
		}
			
		// Starts the camera
		camTexture.Play ();


		while (true) {

			if (camTexture.didUpdateThisFrame && camTexture.width > 16) {
				if (onCameraChange != null)
					onCameraChange();
                InputCamera = oldCameraIndex;

                //Debug.Log ("width " + camTexture.width + " height " + camTexture.height + " fps " + camTexture.requestedFPS);
                //Debug.Log ("videoRotationAngle " + camTexture.videoRotationAngle + " videoVerticallyMirrored " + camTexture.videoVerticallyMirrored + " isFrongFacing " + webCam.isFrontFacing);
                OnDestory();

                colors = new Color32[camTexture.width * camTexture.height];
                int h = camTexture.height;
                int w = camTexture.width;

                rgbaMat = new Mat (h, w, CvType.CV_8UC4);
                grayMat = new Mat (h, w, CvType.CV_8UC1);
				grayBGMat = new Mat (h, w, CvType.CV_8UC1);
				diffMat = new Mat (h, w, CvType.CV_8UC1);
				grayMatOld = new Mat (h, w, CvType.CV_8UC1);
				accamulator = new Mat (h, w, CvType.CV_8UC1, new Scalar(0,0,0,255));
                resultMap = new Mat(h, w, CvType.CV_8UC1, new Scalar(0, 0, 0, 255));

                texture = new Texture2D (w, h, TextureFormat.RGBA32, false);
                texture_2 = new Texture2D(w, h, TextureFormat.RGBA32, false);

                if (detectOut != null)
					detectOut.material.mainTexture = texture;
                if (debugOutTex != null)
                    debugOutTex.material.mainTexture = texture_2;
				//camOut.material.mainTexture = camTexture;
				//updateLayout ();

				//screenOrientation = Screen.orientation;
				initDone = true;

				break;
			} else {
				yield return 0;
			}
		}
	}

	// Update is called once per frame
	void Update ()
	{
		
		if (!initDone)
			return;

		if (camTexture.didUpdateThisFrame) {

			if (grayMatOld != null) {

                Utils.webCamTextureToMat(camTexture, rgbaMat, colors);

                Imgproc.cvtColor (rgbaMat, grayMat, Imgproc.COLOR_RGB2GRAY);
					
				if (mode == Mode.FrameByBG) {

					if (callNewBG) {
						grayMat.copyTo (grayBGMat);
						callNewBG = false;
						bgSetted = true;
					} else {
						if (grayBGMat != null && bgSetted) {
							FindDifference (new Mat[] { grayMat, grayBGMat });
						}
					}

				} else if(mode == Mode.FrameByFrame) {
					FindDifference (new Mat[] { grayMat, grayMatOld });
                    grayMat.copyTo (grayMatOld);
				} else
                {
                    if (bgSetted) {
                        FindDifference(new Mat[]{ grayMat, grayBGMat});
                        Core.addWeighted(grayBGMat, 1-accomulateBgSize, grayMat, accomulateBgSize, 0, grayBGMat);
                    }
                    else
                    {
                        bgSetted = true;
                        grayMat.copyTo(grayBGMat);
                    }
                }
                
                if (DebugOut && detectOut != null && accamulator.width() == texture.width) {

                    
                    Utils.matToTexture2D(resultMap,texture);
                    Utils.matToTexture2D(diffMat, texture_2);

                }
                //frame = 0;

            }
		}

	}

	void FindDifference (Mat [] arrMat)
	{

        
        //Mat diffContoursMat = new Mat(arrMat[0].size(), arrMat[0].type());
        //diffMat.setTo(new Scalar(0));

        for (int i = 0; i < arrMat.Length-1; i++) {
            if (blured)
            {
                Imgproc.GaussianBlur(arrMat[i], arrMat[i], new Size(size, size), 0);
                Imgproc.GaussianBlur(arrMat[i+1], arrMat[i+1], new Size(size, size), 0);

            }

            Core.absdiff(arrMat[i],arrMat[i+1], diffMat);
        }
        
        Imgproc.threshold(diffMat, diffMat, threshold, 250, Imgproc.THRESH_BINARY);
        Imgproc.dilate(diffMat, diffMat, new Mat(), new Point(-1, -1), 4);

        contours.Clear();
        rects.Clear();
        Imgproc.findContours(diffMat, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        Core.multiply(accamulator, new Scalar(accomulateDelay), accamulator);

        for (int i = 0; i < contours.Count; i++)
        {

            RotatedRect rect = Imgproc.minAreaRect(new MatOfPoint2f(contours[i].toArray()));

            if (rect.size.area() >= minHullSize * CameraSize.area())
            {

                rects.Add(rect);

                switch (approximateMode)
                {
                    case ApproximateMode.Convex:
                        {
                            List<Point> pts = MathConvex(contours[i]);
                            Imgproc.fillConvexPoly(accamulator, new MatOfPoint(pts.ToArray()), new Scalar(255));
                        }
                        break;
                    case ApproximateMode.Rect:
                        {
                            Point[] ps = new Point[4];
                            rect.points(ps);
                            Imgproc.fillConvexPoly(accamulator, new MatOfPoint(ps), new Scalar(255));
                            break;
                        }
                    case ApproximateMode.Elipse:
                        {
                            MatOfPoint elMat = new MatOfPoint();
                            Imgproc.ellipse2Poly(rect.center, new Size(rect.size.width * 0.7, rect.size.height * 0.7),
                                (int)rect.angle, 0, 360, 16, elMat);
                            //Core.ellipse(accamulator, rect, new Scalar(255),4);
                            Imgproc.fillConvexPoly(accamulator, elMat, new Scalar(255));
                            break;
                        }

                }
            }
        }

        Imgproc.threshold(accamulator, resultMap, 25, 255, Imgproc.THRESH_BINARY);

    }

    public List<Point> MathConvex(MatOfPoint contour)
    {
        
        MatOfInt hullInt = new MatOfInt();
        Imgproc.convexHull(contour, hullInt);


        List<Point> pointMatList = contour.toList();
        List<int> hullIntList = hullInt.toList();
        List<Point> hullPointList = new List<Point>();

        for (int j = 0; j < hullIntList.Count; j++)
        {
            hullPointList.Add(pointMatList[hullIntList[j]]);
        }
        
        return hullPointList ;
    }

    public void OnDestory()
    {
        if (rgbaMat != null)
            rgbaMat.Dispose();
        if (grayMat != null)
            grayMat.Dispose();
        if (grayMatOld != null)
            grayMatOld.Dispose();
        if (grayBGMat != null)
            grayBGMat.Dispose();
        if (diffMat != null)
            diffMat.Dispose();
    }

    public void NewBGImage(){
		callNewBG = true;
	}

	void OnDisable ()
	{
		camTexture.Stop ();
	}
}
