﻿using UnityEngine;
using System.Collections;
using OpenCVForUnity;
using System.Collections.Generic;

public class GetMotionFaster : MonoBehaviour
{

    public enum Mode { FrameByFrame, FrameByBG };
    public enum Detect { Biggest, All, Selections };

    WebCamTexture camTexture;
    WebCamDevice webCam;

    public Size CameraSize = new Size(640, 480);
    Mat rgbaMat;
    Mat grayMatOld;
    Mat grayMat;
    Mat grayBGMat;
    Mat diffMat;
    Mat outMat;

    Color32[] colors;

    Texture2D texture;

    bool initDone = false;

    //public Renderer camOut;
    public int InputCamera = 0;

    public Renderer inputCamera;
    public Renderer detectOut;

    public bool DebugOut = true;

    public Mode mode = Mode.FrameByFrame;
    public Detect detectMode = Detect.Selections;
    
    public Transform screen;
    [Range(1, 255)]
    public int thresholdUp = 25;
    [Range(1, 255)]
    public int thresholdDown = 25;
    [Range(1, 10)]
    public int cannyAperture = 3;

    public bool blured = true;
    public int size = 11;
    [Space(10)]
    [Range(0, 1f)]
    public float minHullSize = 0.002f;
    public bool ignoreLockedCountors = true;
    public bool mergeNearRects = true;
    [Range(0, 1)]
    public float nearFactor = 0.5f;

    private List<MatOfPoint> contours = new List<MatOfPoint>();
    private List<MatOfPoint> oldContours = new List<MatOfPoint>();

    private List<RotatedRect> rects = new List<RotatedRect>();

    private List<CoreUtils.MotionVector> motionVectors = new List<CoreUtils.MotionVector>();

    bool callNewBG = false;
    bool bgSetted = false;

    public delegate void OnCameraChange();
    public static OnCameraChange onCameraChange;

    public delegate void OnDetect(CoreUtils.MotionVector[] vectors);
    public static OnDetect onDetect;

    private int oldCameraIndex = -1;

    public void Start()
    {
        if (screen == null)
            screen = transform;

        if (WebCamTexture.devices.Length > 0)
            StartCoroutine(init(InputCamera));
    }

    public void ChangeInputCamera(int index)
    {
        StartCoroutine(init(index));

    }
    private IEnumerator init(int cameraIndex)
    {
        if (camTexture != null && cameraIndex % WebCamTexture.devices.Length != oldCameraIndex)
        {
            camTexture.Stop();
            initDone = false;
            rgbaMat.Dispose();
        }

        if (WebCamTexture.devices.Length > 0)
        {
            oldCameraIndex = cameraIndex % WebCamTexture.devices.Length;
            webCam = WebCamTexture.devices[oldCameraIndex];
            camTexture = new WebCamTexture(webCam.name, (int)CameraSize.width, (int)CameraSize.height);
            Debug.Log(webCam.name);
        }
        else {
            yield return null;
        }

        // Starts the camera
        camTexture.Play();


        while (true)
        {

            if (camTexture.didUpdateThisFrame && camTexture.width > 16)
            {
                if (onCameraChange != null)
                    onCameraChange();
                InputCamera = oldCameraIndex;

                //Debug.Log ("width " + h + " height " + w + " fps " + camTexture.requestedFPS);
                //Debug.Log ("videoRotationAngle " + camTexture.videoRotationAngle + " videoVerticallyMirrored " + camTexture.videoVerticallyMirrored + " isFrongFacing " + webCam.isFrontFacing);
                OnDestory();
                int h = camTexture.height;
                int w = camTexture.width;

                colors = new Color32[h * w];
                rgbaMat = new Mat(h, w, CvType.CV_8UC4);
                grayMat = new Mat(h, w, CvType.CV_8UC1);
                grayBGMat = new Mat(h, w, CvType.CV_8UC1);
                diffMat = new Mat(h, w, CvType.CV_8UC1);
                grayMatOld = new Mat(h, w, CvType.CV_8UC1);
                outMat = Mat.zeros(rgbaMat.size(), CvType.CV_8UC1);

                texture = new Texture2D(w, h, TextureFormat.RGBA32, false);

                if (detectOut != null)
                    detectOut.material.mainTexture = texture;
                if (inputCamera != null)
                    inputCamera.material.mainTexture = camTexture;

                //camOut.material.mainTexture = camTexture;
                //updateLayout ();

                //screenOrientation = Screen.orientation;
                initDone = true;

                break;
            }
            else {
                yield return 0;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (!initDone)
            return;

        if (camTexture.didUpdateThisFrame)
        {

            if (grayMatOld != null)
            {

                Utils.webCamTextureToMat(camTexture, rgbaMat, colors);
                Imgproc.cvtColor(rgbaMat, grayMat, Imgproc.COLOR_RGB2GRAY);

                if (mode == Mode.FrameByBG)
                {

                    if (callNewBG)
                    {
                        grayMat.copyTo(grayBGMat);
                        callNewBG = false;
                        bgSetted = true;
                    }
                    else {
                        if (grayBGMat != null && bgSetted)
                        {
                            FindDifference(grayMat, grayBGMat);
                        }
                    }

                }
                else {
                    FindDifference(grayMat, grayMatOld);
                    grayMat.copyTo(grayMatOld);
                }

                if (DebugOut && detectOut != null && outMat.width() == texture.width)
                {
                    Utils.matToTexture2D(outMat, texture);
                }
                //frame = 0;

            }
        }

    }

    List<List<MatOfPoint>> history = new List<List<MatOfPoint>>();

    bool first_loop = true;
    void FindDifference(Mat first, Mat second)
    {
        Mat copy = first.clone();

        Imgproc.blur(outMat, outMat, new Size(size, size));
        Imgproc.blur(copy, copy, new Size(size, size));
        /*
        Core.multiply(first, new Scalar(0.3, 0.3, 0.3,1), first);
        Core.multiply(outMat, new Scalar(0.7, 0.7, 0.7,1), outMat);
        Core.add(first, outMat, outMat);
        */
        //Core.invert(first, first);
        Imgproc.accumulateWeighted(outMat, first, 0.99);
        
        //second.copyTo(outMat);
        
        ///Core.absdiff(outMat, copy, outMat);
        //first.copyTo(outMat);
    }
    public List<MatOfPoint> MathConvex(MatOfPoint contour)
    {

        MatOfInt hullInt = new MatOfInt();
        Imgproc.convexHull(contour, hullInt);


        List<Point> pointMatList = contour.toList();
        List<int> hullIntList = hullInt.toList();
        List<Point> hullPointList = new List<Point>();

        for (int j = 0; j < hullIntList.Count; j++)
        {
            hullPointList.Add(pointMatList[hullIntList[j]]);
        }

        MatOfPoint hullPointMat = new MatOfPoint();

        hullPointMat.fromList(hullPointList);

        List<MatOfPoint> hullPoints = new List<MatOfPoint>();

        hullPoints.Add(hullPointMat);

        return hullPoints;
    }

    public void OnDestory()
    {
        if (rgbaMat != null)
            rgbaMat.Dispose();
        if (grayMat != null)
            grayMat.Dispose();
        if (grayMatOld != null)
            grayMatOld.Dispose();
        if (grayBGMat != null)
            grayBGMat.Dispose();
        if (diffMat != null)
            diffMat.Dispose();
    }

    public void NewBGImage()
    {
        callNewBG = true;
    }

    void OnDisable()
    {
        camTexture.Stop();
    }
}
