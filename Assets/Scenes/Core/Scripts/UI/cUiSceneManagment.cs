﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class cUiSceneManagment : MonoBehaviour {

    //Main_UI/SceneSelector/ScenesList_Veiwport/Content

    public bool floatDescription = false;
    public RectTransform sceneListContent;
    public RectTransform itemPrefab;
    public RectTransform descriptionPanel;

    private List<Toggle> itemsList = new List<Toggle>();


    Text descriptionPanelName;
    Text descriptionPanelDescription;
    CanvasGroup descriptionPanelGroup;

    void Start () {

        StartCoroutine(UpdateListContent());

        descriptionPanelName = descriptionPanel.FindChild("Name").GetComponent<Text>();
        descriptionPanelDescription = descriptionPanel.FindChild("Description").GetComponent<Text>();
        descriptionPanelGroup = descriptionPanel.GetComponent<CanvasGroup>();
    }

    IEnumerator UpdateListContent()
    {
        while (GlobalVariables.core_SceneManager == null)
        {
            yield return null;
        }

        SceneDescription[] list = GlobalVariables.core_SceneManager.scenes.descriptions;

        foreach (SceneDescription s in list)
        {
            RectTransform item = Instantiate(itemPrefab) as RectTransform;
            item.SetParent(sceneListContent);
            item.Find("Background/Label").GetComponent<Text>().text = s.name;
            itemsList.Add(item.GetComponent<Toggle>());
            item.GetComponent<cUiListItemScript>().linkedScene = s;
            item.GetComponent<cUiListItemScript>().onItemMouseEnter += OnToogleMouseEnter;
            item.GetComponent<cUiListItemScript>().onItemMouseExit += OnToogleMouseExit;


        }
    }


    bool needClose = false;

    //bool faded = true;
    IEnumerator DescriptionFadeUp()
    {

       // StopCoroutine("DescriptionFadeDown");
        descriptionPanel.gameObject.SetActive(true);
        for (float a = descriptionPanelGroup.alpha; a <1; a += 0.1f)
        {
            descriptionPanelGroup.alpha = a;
            yield return null;
        }
    }

    IEnumerator DescriptionFadeDown()
    {
        //StopCoroutine("DescriptionFadeUp");
        for (float a = descriptionPanelGroup.alpha; a >= 0; a -= 0.1f)
        {
            descriptionPanelGroup.alpha = a;
            yield return null;
        }
        needClose = false;
        descriptionPanel.gameObject.SetActive(false);

    }

    IEnumerator DescriptionPanelCloser()
    {
        needClose = true;
        yield return new WaitForSeconds(2f);
        if (needClose)
        {
            StartCoroutine(DescriptionFadeDown());
            
        }
    }

    public void OnToogleMouseEnter(SceneDescription d)
    {
        if (!floatDescription)
            return;

        needClose = false;
        descriptionPanelName.text = d.name;
        descriptionPanelDescription.text = d.description;
        StartCoroutine(DescriptionFadeUp());
    }

    public void OnToogleMouseExit(SceneDescription d)
    {
        if (!floatDescription)
            return;

        StartCoroutine(DescriptionPanelCloser());
    }
    
    void Update () {

        if (descriptionPanel.gameObject.activeSelf && !needClose && floatDescription)
        {
            descriptionPanel.anchoredPosition = Input.mousePosition;
        }
	}

    //Buttons SelectAll and DeselectAll
    public void OnAllButtonsClicked(bool on)
    {
        foreach(Toggle t in itemsList)
        {
            t.isOn = on;
        }
    }
}
