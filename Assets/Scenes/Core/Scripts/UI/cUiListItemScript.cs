﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class cUiListItemScript : MonoBehaviour {

    public SceneDescription linkedScene;

    public delegate void OnTItemMouseEnter(SceneDescription descr);
    public OnTItemMouseEnter onItemMouseEnter;

    public delegate void OnTItemMouseExit(SceneDescription descr);
    public OnTItemMouseEnter onItemMouseExit;
    
    EventTrigger selfEventTriger;
        
	void Start () {

        selfEventTriger = GetComponent<EventTrigger>();
        if (selfEventTriger == null)
            selfEventTriger = gameObject.AddComponent<EventTrigger>();

        EventTrigger.Entry e = new EventTrigger.Entry();
        e.eventID = EventTriggerType.PointerEnter;

        e.callback.AddListener((data) => {
            CallEventEnter(data);
        });

        selfEventTriger.triggers.Add(e);

        e = new EventTrigger.Entry();
        e.eventID = EventTriggerType.PointerExit;

        e.callback.AddListener((data) => {
            CallEventExit(data);
        });
        selfEventTriger.triggers.Add(e);
	}
	
	// Update is called once per frame
    void CallEventEnter(BaseEventData data)
    {
        if(onItemMouseEnter != null){
            onItemMouseEnter(linkedScene);
        }
    }
    void CallEventExit(BaseEventData data)
    {
        if (onItemMouseExit != null)
        {
            onItemMouseExit(linkedScene);
        }
    }
}
