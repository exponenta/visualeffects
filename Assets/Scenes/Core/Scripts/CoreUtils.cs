﻿using UnityEngine;
using System.Collections;
using OpenCVForUnity;
using System.Collections.Generic;

public static class CoreUtils {

	[System.Serializable]
	public class MotionVector
	{
		public Vector3 Pos;
		public Vector2 Size;
        public float Angle;

		public MotionVector ()
		{
			Pos = Vector3.zero;
			Size = Vector2.zero;
            Angle = 0;
		}

		public MotionVector (Vector3 pos, float angle, Vector2 size)
		{
			Pos = pos;
            Angle = angle;
			Size = size;
		}

		public static MotionVector FromRect(RotatedRect rect, Transform screen, Size resolution){
			return TransformRotateRectToMV(rect, screen, resolution);
		}
	}

	public static MotionVector TransformRotateRectToMV(RotatedRect rect, Transform screen, Size resolution){

		if (rect == null || screen == null)
			return null;
		
		MotionVector ret = new MotionVector();

		Vector3 localPos = PointToVector (rect.center);
		localPos.x =  - (0.5f - localPos.x / (float)resolution.width);
		localPos.y = (0.5f - localPos.y / (float)resolution.height);

		ret.Pos = screen.TransformPoint (localPos);

        //Debug.Log ("Point:" + rect.center.ToString () + " Vector:" + localPos.ToString ());
        //TODO rescale size!!

        double kX = (screen.localScale.x / resolution.width);
        double kY = (screen.localScale.y / resolution.height);

        double HeightOnX = rect.size.height * Mathf.Cos((float)rect.angle * Mathf.Deg2Rad) * kX;
        double HeightOnY = rect.size.height * Mathf.Sin((float)rect.angle * Mathf.Deg2Rad) * kY;


        double WidthOnX = rect.size.width * Mathf.Sin((float)rect.angle * Mathf.Deg2Rad) * kX;
        double WidthOnY = rect.size.width * Mathf.Cos((float)rect.angle * Mathf.Deg2Rad) * kY;

        ret.Size.y = Mathf.Sqrt((float)(HeightOnX * HeightOnX + HeightOnY * HeightOnY));
        ret.Size.x = Mathf.Sqrt((float)(WidthOnX * WidthOnX + WidthOnY * WidthOnY));

        ret.Angle = (float)rect.angle;
        //ret.Size.Set ((float)(rect.size.width / resolution.width), (float)(rect.size.height / resolution.height));
		//ret.Dir =;
		return ret;
	}

	public static Vector2 PointToVector(Point p) {
		return new Vector2 ((float)p.x, (float)p.y);
	}

	public static Point VectorToPoint(Vector2 v){
		return new Point ((double)v.x, (double)v.y);
	}
	public static Point VectorToPoint(Vector3 v){
		return new Point ((double)v.x, (double)v.y);
	}

	public static RotatedRect MergeRects(RotatedRect[] rects){

		if (rects.Length == 0)
			return null;

		List<Point> vects = new List<Point>();

		foreach(RotatedRect r in rects){

			Point[] ps = new Point[4];
			r.points (ps);
			vects.AddRange (ps);
		}

		return Imgproc.minAreaRect (new MatOfPoint2f (vects.ToArray ()));
	}

}
