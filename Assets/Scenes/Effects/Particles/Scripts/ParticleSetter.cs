﻿using UnityEngine;
using System.Collections;

public class ParticleSetter : MonoBehaviour {

	public Transform[] prefabs;

	public int objectMaxCount = 100;
	public int objectsMinCount = 50;

	public float speedRundomizer = 0.5f;
	public bool randomInstance = true;
	public Vector2 objectsArea = new Vector2 (2.5f, 1f);

	void Start () {
		InstancePrefabs ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void InstancePrefabs() {

		int count = Random.Range (objectsMinCount, objectMaxCount);

		if (!randomInstance)
			count = Mathf.RoundToInt ((float)count / prefabs.Length) * prefabs.Length;
		
		for (int i = 0; i < count; i++) {

			Transform prefab;
			if (randomInstance)
				prefab = prefabs [Random.Range (0, prefabs.Length)];
			else {
				prefab = prefabs [i % prefabs.Length];
			}

			Transform np = Instantiate (prefab);

			np.GetComponent<ParticleController> ().enabled = true;
			np.GetComponent<ParticleController> ().followSpeed *= Random.Range (speedRundomizer, 1f);
			np.SetParent (transform);
			np.name = prefab.name + "_" + i;

			float rx = Random.Range (-objectsArea.x, objectsArea.x);
			float ry = Random.Range (-objectsArea.y, objectsArea.y);

			np.position = new Vector3 (rx, ry, 0);
		}
	}
}
