﻿using UnityEngine;
using System.Collections;

public class ParticleController : MonoBehaviour {

	public GetMotionVectors controller;

	public bool lockZ = true;
	public float followSpeed = 10f;
	public float followDistance = 1f;

	public Transform targetHolder;

	private Vector3 target;
	private Transform targetObj;

	void Start () {

		if (controller == null)
			controller = GameObject.FindGameObjectWithTag ("CameraController").GetComponent<GetMotionVectors> ();

		if (controller != null) {
			GetMotionVectors.onDetect += updateVector;
		}

		if (targetHolder != null)
			targetObj = Instantiate (targetHolder) as Transform;
	
	}
	
	// Update is called once per frame
	void Update () {

		if (target != null && Vector3.Distance(transform.position, target) <  followDistance) {
			
			Vector3 pos = Vector3.Lerp (transform.position, target, Time.deltaTime * followSpeed);

			transform.position = pos;
		}
	}


	void updateVector(CoreUtils.MotionVector[] v){

		if (v.Length > 1) {

			float min = 10000f;
			Vector3 minPos = v[0].Pos;
			for (int i = 0; i < v.Length; i++) {
				v [i].Pos.z = transform.position.z;
				float d = Vector3.Distance (transform.position, v [i].Pos);

				if (d < min) {
					min = d;
					minPos = v [i].Pos;
				}
			}

			target = minPos;
		} else {
			target = v [0].Pos;
		}
		if(lockZ)
			target.z = transform.position.z;
		if (targetObj != null)
			targetObj.position = target;
	}
}
