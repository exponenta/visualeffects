﻿using UnityEngine;
using System.Collections;
using System;

public class debugTarget : MonoBehaviour {

	void Start () {

		GetMotionVectors.onDetect += SetPos;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    class Comp : IComparer
    {
        public Vector3 target;
        public int Compare(object x, object y)
        {
            CoreUtils.MotionVector mX = x as CoreUtils.MotionVector;
            CoreUtils.MotionVector mY = y as CoreUtils.MotionVector;

            return (int)Mathf.Sign(Vector3.Distance(mX.Pos, target) - Vector3.Distance(mY.Pos, target));
        }
    }

    Comp cmp = new Comp();
    void SetPos(CoreUtils.MotionVector[] v)
	{
        CoreUtils.MotionVector[] clone = (CoreUtils.MotionVector[])v.Clone();
        cmp.target = transform.position;
        System.Array.Sort(clone, cmp);

		transform.position = new Vector3 (clone [0].Pos.x, clone [0].Pos.y, transform.position.z);
        transform.localScale = new Vector3(clone[0].Size.x, clone[0].Size.y, 1);
        transform.rotation = Quaternion.AngleAxis(clone[0].Angle, -Vector3.forward);
	}
}
