﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

public class VisualEffectsHelper : MonoBehaviour {

    public static string sceneDirectory = "\\Scenes\\Effects";
    public static string desFileName = "\\Resources\\SceneDescriptions.json";

    [MenuItem("Project Tools / Update Scenes Description")]
    public static void UpdateScenes()
    {
        string path = Application.dataPath;

        List<SceneDescription> descriptions = new List<SceneDescription>();

        if (Directory.Exists(path + sceneDirectory))
        {
            string[] sub = Directory.GetDirectories(path + sceneDirectory);
           
            foreach(string d in sub)
            {
                string[] des = Directory.GetFiles(d, "description*.json", SearchOption.TopDirectoryOnly);
                string[] scenes = Directory.GetFiles(d, "*.unity", SearchOption.TopDirectoryOnly);
                

                if (scenes.Length == 0 && des.Length == 0)  {
                    Debug.LogWarning(d + "has not Scene or Description files. Ignore!!");
                    continue;
                }
                
                if(des.Length > 0)
                {

                    Debug.Log("[Interate description]");
                    foreach (string de in des) {
                        string str = (new StreamReader(de)).ReadToEnd();
                        Debug.Log(str);
                        SceneDescription description = SceneDescription.FromJson(str);
                        if(description != null)
                        {
                            if(!File.Exists(d + "\\" + description.sceneFileName)){
                                Debug.LogWarning("Scene file " + description.sceneFileName + " not found. Ignore it!");
                                continue;
                            }
                            descriptions.Add(description);
                        } else
                        {
                            Debug.LogWarning("Description " + de + " has not valid format. Ignore it!");
                            continue;
                        }
                    }
                }else
                {
                    foreach (string sc in scenes)
                    {
                        SceneDescription description = new SceneDescription();

                        int start = sc.LastIndexOf("\\")+1;
                        string trim_scene = sc.Substring(start);

                        description.name = trim_scene.Replace(".unity", "");
                        description.sceneFileName = trim_scene;

                        string s = description.ToJson();
                        if (s.Length > 4)
                        {
                            File.WriteAllText(d + "/description.json", s);
                            
                        }

                        descriptions.Add(description);
                    }
                }
            }
        
        if(descriptions.Count > 0)
            {
                string s = (new ScenesDescriptionList(descriptions.ToArray())).ToJson();
                File.WriteAllText(path + desFileName, s);

                Debug.Log("Descriptions Updated!!");
            }
            else
            {
                Debug.Log("Descriptions not found");
            }
        } else {
            Debug.LogError(path + "not found!");
        }
    }
}
